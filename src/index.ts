import {CommunicationJob} from "./jobs/CommunicationJob";
import {RPCServer} from "./mqlistener";
import {XmlManager} from "./managers/DataManagers/XmlManager";

console.log(`Running enviroment ${process.env.NODE_ENV || "dev"}`);
const job = new CommunicationJob("communicationJob");
XmlManager.getSkills();

new RPCServer().start(job);