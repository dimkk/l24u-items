export interface IDiabloSkill {
    name:string;
    lvl:number;
    icon:string;
    id:number;
}

export interface IDiabloItem {
    name:string;
    currentRandom:number;
    icon:string;
    objId:number;
    skills:IDiabloSkill[];
    bodyPart:number;
    isMagic:boolean;
    currentPrice?:number;
    grade?:string;
}

export interface IDb {
    playerId;
    currentStartLine:number;
    data;
    selectedItem?;
    selectedSkill?;
    randomSkills?;
    availSkills?;
    currencyId?;
    currentPrice?;
    isAdmin?;
    newSkill?;
}

export interface IHelpHtmlData {
    currentStartLine?:number;
    selectedItem?:IDiabloItem;
    randomSkills?:IDiabloSkill[];
    items?:any;
    error?:string;
}

export interface IAdminHtmlData extends IHelpHtmlData {
    selectedSkill?: IDiabloSkill;
    newSkill?: IDiabloSkill;
}

export interface IMainHtmlData extends IHelpHtmlData {
    money:number;
    currencyId:number;
    selectedSkill?:IDiabloSkill;
}

export interface IEnchantHtmlData extends IMainHtmlData {
    currentPrice:number;
    successChance:string;
    failChance:string;
    del:string;
    uplvl:number;
    halfPrice:number;
    showResult:boolean;
    delShow:boolean;
    showFail:boolean;
    showSuccess:boolean;
    checkUpgradeDel:string;
}