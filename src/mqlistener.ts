import * as amqp from "amqplib";
import { IMqConfiguration } from "./configurations";
import * as nconf from "nconf";
import { WrapperAction } from "./common/WrapperAction";
import * as Configs from "./configurations";
import * as w from "./common/Logger";
import {CommunicationJob} from "./jobs/CommunicationJob";

export class RPCServer {
    configs: any;
    connectionUrl: string;
    open: any;
    q: string;
    /**
     * RPC mq server
     */
    constructor() {
        this.configs = Configs.getCfg().get("mq");
        this.connectionUrl = `amqp://${this.configs.RABBITMQ_LOGIN}:${this.configs.RABBITMQ_PASS}@${this.configs.RABBITMQ_ADDRESS}`;
        this.open = amqp.connect(this.connectionUrl);
        this.q = this.configs.RABBITMQ_QUEUE + "statsRnd";
        w.getLogger().info(this.configs.RABBITMQ_QUEUE);
    }

    start(action: WrapperAction) {
        this.open
            .then((conn) => {
                process.on('exit', function() { conn.close(); w.getLogger().info('mq connection closed') });
                return conn.createChannel();
            })
            .then((ch) => {
                ch.assertQueue(this.q, { durable: false });
                ch.prefetch(1);
                w.getLogger().info(' [x] Awaiting RPC requests ' + this.q + ' for ' + action.getActionName());
                ch.consume(this.q, function reply(msg: amqp.Message) {
                    const job = new CommunicationJob("communicationJob");
                    var n = msg.content.toString();
                    var nToShow = n.length > 100 ? nToShow = n.substring(0, 100) : n;
                    w.getLogger().info(" [.] recieved", nToShow);
                    var response = job.doRealWork(n);
                    var responseToShow = response.length > 100 ? responseToShow = response.substring(0, 100) : response;
                    w.getLogger().info(" [x] response ", responseToShow);
                    ch.sendToQueue(
                        msg.properties.replyTo || "",
                        new Buffer(response),
                        { correlationId: msg.properties.correlationId || "" }
                    );
                    ch.ack(msg);
                });
                // let obj = {
                //     section:"statsRnd",
                //     method:"test",
                //     args:["arg1", "arg2"],
                //     hash:{hasKey1:"hashVal1", hashKey2:"hashVal2"}
                // };
                // ch.sendToQueue(this.q, new Buffer(JSON.stringify(obj)));
            });
    }
}
