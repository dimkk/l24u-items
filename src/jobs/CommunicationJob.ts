import { WrapperAction } from "../common/WrapperAction";
import { ICommunicationObject } from "../common/ICommunicationObject";
import { HtmlManager } from "../managers/HtmlManagers/HtmlManager";
import { HtmlItemsListManager } from "../managers/HtmlManagers/HtmlItemsListManager";
import { HtmlHelpManager } from "../managers/HtmlManagers/HtmlHelpManager";
import { XmlManager } from "../managers/DataManagers/XmlManager";
import { HtmlEnchantManager } from "../managers/HtmlManagers/HtmlEnchantManager";
import { HtmlAdminManager } from "../managers/HtmlManagers/HtmlAdminManager";
import { StatsRndParamsManager } from "../managers/DataManagers/StatsRndParamsManager";

export class CommunicationJob extends WrapperAction {
    manager:HtmlManager;
    /**
     * Use this to communicate
     */
    constructor(name: string) {
        super(name);
    }

    doWork(params: string): string {
        let result = "";
        let obj: ICommunicationObject;
        let sk;
        try {
            obj = JSON.parse(params);
        } catch (err) {
            console.error(err);
            return "bad json";
        }
        if (obj.section === "statsRnd") {
            if (obj.method === "getData" && obj.args.length > 0) {
                result = XmlManager.getXml(obj.args[0]);
                sk = XmlManager.getSkills();
            } else if (obj.method === "getStatsRndParams") {
                result = StatsRndParamsManager.getParams();
            } else if (obj.method === "showHelp" || obj.method === "setHelpItem") {
                result = new HtmlHelpManager(obj.hash.playerId).getHtml(obj);
            } else if (
                obj.method === "showEnchant"
                || obj.method === "setEnchantItem"
                || obj.method === "setEnchantSkill"
                || obj.method === "upgradeSkill"
                || obj.method === "prepUpgradeSkill"
                || obj.method === "enchantModifyPrice"
                || obj.method === "enchantSetPrice"
                ) {
                result = new HtmlEnchantManager(obj.hash.playerId).getHtml(obj);
            } else if (
                obj.method === "showAdmin" && obj.hash.isAdmin === "true"
                || obj.method === "setAdminItem"
                || obj.method === "setAdminSkill"
                || obj.method === "setAdminNewSkill"
                || obj.method === "setAdminNewSkillLvl"
                || obj.method === "showAdminSetSkillAndLvl"
                || obj.method === "showAdminAddSkillAndLvl"
                || obj.method === "showAdminRemoveSkill"
                ) {
                result = new HtmlAdminManager(obj.hash.playerId).getHtml(obj);
            } else if (
                obj.method === "itemsList"
                || obj.method === "itemsList"
                || obj.method === "itemsListSetItem"
                || obj.method === "itemsListEqUneq"
                || obj.method === "itemsListDrop"
                || obj.method === "itemsListSell"
                || obj.method === "itemsListWh"
                ) {
                result = new HtmlItemsListManager(obj.hash.playerId).getHtml(obj);
            } else if (
                obj.method === "showMain"
                || obj.method === "setSkil"
                || obj.method === "setSkill"
                || obj.method === "doRandom"
                || obj.method === "setItem"
            ) {
                result = new HtmlManager(obj.hash.playerId).getHtml(obj);
            }
        }
        return result;
    }
}