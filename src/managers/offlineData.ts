class OfflineData {
    public static rndSkills() {
        return [
            {name:'Item Run Speed Mastery Body', lvl:5, icon:'icon.armor_t62_ul_i00', id:1},
                    {name:'Item Cast Speed Mastery Weapon', lvl:3, icon:'icon.weapon_tarbar_i00', id:2},
                    {name:'Item Attack Speed Mastery Gloves', lvl:7, icon:'icon.armor_t62_g_i00', id:3},
                    {name:'Item Attack Speed Mastery Body', lvl:11, icon:'icon.armor_t62_ul_i00', id:4},
                    {name:'Item Attack Speed Mastery Head', lvl:21, icon:'icon.armor_leather_helmet_i00', id:5},
                    {name:'Item Run Speed Mastery Body', lvl:5, icon:'icon.armor_t62_ul_i00', id:6},
                    {name:'Item Attack Speed Mastery Gloves', lvl:7, icon:'icon.armor_t62_g_i00', id:7},
                    {name:'Item Attack Speed Mastery Body', lvl:11, icon:'icon.armor_t62_ul_i00', id:8},
                    {name:'Item Attack Speed Mastery Head', lvl:21, icon:'icon.armor_leather_helmet_i00', id:9}
        ]
    }

    public static items() {
        return [
            { objId : '268532602', icon:'icon.icon.etc_debris_i00', name:'hz1', currentPrice:500, skills: [
                    {name:'Item Run Speed Mastery Body', lvl:5, icon:'icon.armor_t62_ul_i00', id:1},
                    {name:'Item Cast Speed Mastery Weapon', lvl:3, icon:'icon.weapon_tarbar_i00', id:2},
                    {name:'Item Attack Speed Mastery Gloves', lvl:7, icon:'icon.armor_t62_g_i00', id:3},
                    {name:'Item Attack Speed Mastery Body', lvl:11, icon:'icon.armor_t62_ul_i00', id:4},
                    {name:'Item Attack Speed Mastery Head', lvl:21, icon:'icon.armor_leather_helmet_i00', id:5}
                ] },
            { objId : '268532601', icon:'icon.icon.etc_dragon_egg_i02', currentPrice:500, name:'hz12', skills: [
                    {name:'Item Run Speed Mastery Body', lvl:5, icon:'icon.armor_t62_ul_i00', id:1},
                    {name:'Item Attack Speed Mastery Gloves', lvl:7, icon:'icon.armor_t62_g_i00', id:2},
                    {name:'Item Attack Speed Mastery Body', lvl:11, icon:'icon.armor_t62_ul_i00', id:3},
                    {name:'Item Attack Speed Mastery Head', lvl:21, icon:'icon.armor_leather_helmet_i00', id:4}
                ] },
            { objId : '268532600', icon:'icon.icon.etc_crystal_white_i00', currentPrice:500, name:'hz13', skills: [
                    {name:'Item Run Speed Mastery Body', lvl:5, icon:'icon.armor_t62_ul_i00', id:1},
                    {name:'Item Cast Speed Mastery Weapon', lvl:3, icon:'icon.weapon_tarbar_i00', id:2},
                    {name:'Item Attack Speed Mastery Head', lvl:21, icon:'icon.armor_leather_helmet_i00', id:3}
                ]},
            { objId : '268534472', icon:'icon.icon.etc_spirit_bullet_gold_i00', currentPrice:500, name:'hz1333', skills: [
                    {name:'Item Run Speed Mastery Body', lvl:5, icon:'icon.armor_t62_ul_i00', id:1},
                    {name:'Item Cast Speed Mastery Weapon', lvl:3, icon:'icon.weapon_tarbar_i00', id:2},
                    {name:'Item Attack Speed Mastery Gloves', lvl:7, icon:'icon.armor_t62_g_i00', id:3},
                ]},
            { objId : '268534473', icon:'icon.icon.etc_spell_shot_gold_i01', currentPrice:500, name:'hz1555', skills: [
                    {name:'Item Cast Speed Mastery Weapon', lvl:3, icon:'icon.weapon_tarbar_i00', id:1},
                    {name:'Item Attack Speed Mastery Gloves', lvl:7, icon:'icon.armor_t62_g_i00', id:2},
                ]},
            { objId : '268527952', icon:'icon.icon.etc_stone_gray_i00', currentPrice:500, name:'hhhhhh', skills: [
                    {name:'Item Run Speed Mastery Body', lvl:5, icon:'icon.armor_t62_ul_i00', id:1},
                    {name:'Item Cast Speed Mastery Weapon', lvl:3, icon:'icon.weapon_tarbar_i00', id:2},
                    {name:'Item Attack Speed Mastery Gloves', lvl:7, icon:'icon.armor_t62_g_i00', id:3},
                    {name:'Item Attack Speed Mastery Head', lvl:21, icon:'icon.armor_leather_helmet_i00', id:4}
                ] },
            { objId : '268481185', icon:'icon.icon.weapon_monster_i00', currentPrice:500, name:'fffffff', skills: [
                    {name:'Item Attack Speed Mastery Gloves', lvl:7, icon:'icon.armor_t62_g_i00', id:1},
                    {name:'Item Attack Speed Mastery Body', lvl:11, icon:'icon.armor_t62_ul_i00', id:2},
                    {name:'Item Attack Speed Mastery Head', lvl:21, icon:'icon.armor_leather_helmet_i00', id:3}
                ] },
            { objId : '268485073', icon:'icon.icon.armor_t51_u_i00', currentPrice:500, name:'zzzzzzzz' },
            { objId : '268481091', icon:'icon.icon.weapon_dynasty_spear_i00', currentPrice:500, name:'nnnnnnn' },
            { objId : '268939584', icon:'icon.icon.armor_t2000_ul_i00', currentPrice:500, name:'33333333' },
            { objId : '268481195', icon:'icon.icon.armor_t42_b_i00', currentPrice:500, name:'555555555' },
            { objId : '268481197', icon:'icon.icon.armor_t42_u_i00', currentPrice:500, name:'7777777777' },
            { objId : '268534472', icon:'icon.icon.etc_spirit_bullet_gold_i00', currentPrice:500, name:'hz1333' },
            { objId : '268534473', icon:'icon.icon.etc_spell_shot_gold_i01', currentPrice:500, name:'hz1555' },
            { objId : '268527952', icon:'icon.icon.etc_stone_gray_i00', currentPrice:500, name:'hhhhhh' },
            { objId : '268481185', icon:'icon.icon.weapon_monster_i00', currentPrice:500, name:'fffffff' },
            { objId : '268485073', icon:'icon.icon.armor_t51_u_i00', currentPrice:500, name:'zzzzzzzz' },
            { objId : '268481091', icon:'icon.icon.weapon_dynasty_spear_i00', currentPrice:500, name:'nnnnnnn' },
            { objId : '268481091', icon:'icon.icon.weapon_dynasty_spear_i00', currentPrice:500, name:'nnnnnnn' },
            { objId : '268939584', icon:'icon.icon.armor_t2000_ul_i00', currentPrice:500, name:'33333333' },
            { objId : '268481195', icon:'icon.icon.armor_t42_b_i00', currentPrice:500, name:'555555555' },
            { objId : '268481197', icon:'icon.icon.armor_t42_u_i00', currentPrice:500, name:'7777777777' },
            { objId : '268534472', icon:'icon.icon.etc_spirit_bullet_gold_i00', currentPrice:500, name:'hz1333' }
        ]
    }
}