import { ICommunicationObject } from "../../common/ICommunicationObject";
import * as fs from "fs";
import * as handlebars from "handlebars";
import * as _ from "lodash";
import { Db } from "../CommonManagers/DbManager";
import { XmlManager } from "../DataManagers/XmlManager";
import * as Configs from "../../configurations";
import { SkillRndManager } from "../CommonManagers/SkillRndManager";
import { IDiabloItem, IDiabloSkill, IDb, IAdminHtmlData } from '../../entities/interfaces';

export class HtmlAdminManager {
    currentStartLine:number = 0;
    data:IAdminHtmlData;
    obj:ICommunicationObject;
    selectedItem;
    availSkills = [];
    serverItems:any[];
    selectedSkill:IDiabloSkill;
    playerId;
    newSkill: IDiabloSkill;
    /**
     *
     */
    constructor(playerId) {
        this.playerId = playerId;
        let data = Db.get(playerId + "admin");
        if (data) {
            this.currentStartLine = data.currentStartLine;
            this.data = data.data;
            this.selectedItem = data.selectedItem;
            this.availSkills = data.availSkills;
            this.newSkill = data.newSkill;
            this.selectedSkill = data.selectedSkill;
        } else {
            this.data = {} as IAdminHtmlData;
        }
    }
    updateDb() {
        let item = {
            playerId:this.playerId + "admin",
            currentStartLine:this.currentStartLine,
            data:this.data,
            selectedItem:this.selectedItem,
            selectedSkill:this.selectedSkill,
            availSkills:this.availSkills,
            newSkill:this.newSkill
        }
        Db.add(item);
    }
    getHtml(obj: ICommunicationObject): string {
        this.obj = obj;
        if (obj.hash.items) {
            this.serverItems = JSON.parse(obj.hash.items);
        }
        let result = "";
        this.data.error = ""; //obj.hash;
        if (obj.hash.error) {
            this.data.error = obj.hash.error;
        }
        this.setItemsToShow();

        if (obj.method === "setAdminItem" && obj.args.length > 0) {
            this.availSkills = [];
            this.refreshSelectedItem(obj.args[0]);
            this.selectedSkill = null;
            this.newSkill = null;
        }

        if (obj.method === "setAdminSkill" && obj.args.length > 0) {
            let filteredSkills = this.selectedItem != null ? this.selectedItem.skills.filter((el) => {
                return el.id.toString() === obj.args[0].toString();
            }) : [];
            if (filteredSkills.length > 0) {
                 this.selectedSkill = filteredSkills[0];
            } else {
                this.selectedSkill = null;
                this.newSkill = null;
            }
        }
        if (obj.method === "setAdminNewSkill" && obj.args.length > 0) {
            let filteredSkills = this.availSkills != null ? this.availSkills.filter((el) => {
                return el.id.toString() === obj.args[0].toString();
            }) : [];
            if (filteredSkills.length > 0) {
                 this.newSkill = filteredSkills[0];
            }
        }
        if (obj.method === "setAdminNewSkillLvl" && obj.args.length > 0) {
            let newLvl = obj.args[1];
            if (this.newSkill) {
                this.newSkill.lvl = +newLvl;
            }
        }

        if (obj.method === "showAdminSetSkillAndLvl" && obj.args.length > 2) {
            let filteredSkills = this.selectedItem != null ? this.selectedItem.skills.filter((el) => {
                return el.id.toString() === obj.args[2].toString();
            }) : [];
            if (filteredSkills.length > 0) {
                 this.selectedSkill = filteredSkills[0];
            }
        }
        if (obj.method === "showAdminRemoveSkill") {
            this.selectedSkill = null;
        }
        if (obj.method === "showAdminSetSkillAndLvl") {
            this.selectedSkill = null;
            this.newSkill = null;
        }
        if (this.selectedItem) {
            this.refreshSelectedItem(this.selectedItem.objId);
        }
        if (this.selectedItem) {
            this.fillAvailSkills();
        }

        result = this.fillTemplate();
        this.updateDb();
        return result;
    }

    refreshSelectedItem(id, setSkillEmpty?) {
        let clone = _.clone(this.serverItems);
        let filteredItems = clone.filter((el) => {
                return el.objId.toString() === id.toString();
            });
            if (filteredItems.length > 0) {
                let item:IDiabloItem = filteredItems[0];
                this.selectedItem = item;
            } else {
                this.selectedItem = null;
            }
            if (setSkillEmpty) {
                this.selectedSkill = null;
            }
    }

    fillAvailSkills() {
        this.availSkills = SkillRndManager.getAvailSkillsForItem(this.selectedItem, this.selectedSkill);
    }
    setItemsToShow() {
        let topLimit = Math.ceil(this.serverItems.length / 6);
        if (this.obj.args.length > 0 && this.obj.args[0] === "up" && this.currentStartLine > 0) {
            this.currentStartLine--;
        }
        if (this.obj.args.length > 0 && this.obj.args[0] === "down" && this.currentStartLine < topLimit - 2) {
            this.currentStartLine++;
        }

        let clonedItems = _.clone(this.serverItems);
        let chunkedItems = _.chunk(clonedItems, 6);
        let chunkedItemsArray = _.toArray(chunkedItems);
        let shapedItemsArray = chunkedItemsArray.slice(this.currentStartLine, this.currentStartLine + 2);
        this.data["items"] = shapedItemsArray;
    }

    fillTemplate() {
        let result = "";
        let temp = fs.readFileSync("./src/html/admin.html", "utf8");
        let template = handlebars.compile(temp);

        handlebars.registerHelper('lvl', function(data) {
            if (data) {
                return new handlebars.SafeString (", lvl " + data);
            };
        });

        handlebars.registerHelper('num', function numberWithCommas(x) {
            if (x) {
                return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            }
        });

        this.data["selectedItem"] = this.selectedItem;
        this.data["availSkills"] = this.availSkills;
        this.data["selectedSkill"] = this.selectedSkill;
        this.data["newSkill"] = this.newSkill;
        this.data["showSet"] =
            this.selectedSkill != null
            && this.newSkill != null;
        this.data["showAdd"] =
            this.newSkill != null
            && this.selectedItem != null
            && this.selectedSkill === null
            && this.selectedItem.skills.length < 6;
        this.data["showRemove"] = this.selectedSkill != null && this.selectedItem.skills.length > 0;

        result = template(this.data);
        return result;
    }
}