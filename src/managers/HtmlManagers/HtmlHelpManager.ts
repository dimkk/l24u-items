import { ICommunicationObject } from "../../common/ICommunicationObject";
import * as fs from "fs";
import * as handlebars from "handlebars";
import * as _ from "lodash";
import { Db } from "../CommonManagers/DbManager";
import { XmlManager } from "../DataManagers/XmlManager";
import * as Configs from "../../configurations";
import { SkillRndManager } from "../CommonManagers/SkillRndManager";
import { IDb, IHelpHtmlData } from '../../entities/interfaces';

export class HtmlHelpManager {
    currentStartLine:number = 0;
    data:IHelpHtmlData;
    obj:ICommunicationObject;
    selectedItem;
    availSkills = [];
    serverItems:any[];
    playerId;
    /**
     *
     */
    constructor(playerId) {
        this.playerId = playerId;
        let data = Db.get(playerId + "help");
        if (data) {
            this.currentStartLine = data.currentStartLine;
            this.data = data.data;
            this.selectedItem = data.selectedItem;
            this.availSkills = data.randomSkills;
        } else {
            this.data = {} as IHelpHtmlData;
        }
    }
    updateDb() {
        let item = {
            playerId:this.playerId + "help",
            currentStartLine:this.currentStartLine,
            data:this.data,
            selectedItem:this.selectedItem,
            availSkills:this.availSkills
        }
        Db.add(item);
    }
    getHtml(obj: ICommunicationObject): string {
        this.obj = obj;
        if (obj.hash.items) {
            this.serverItems = JSON.parse(obj.hash.items);
        }
        let result = "";
        this.data.error = ""; //obj.hash;
        this.setItemsToShow();

        if (obj.method === "setHelpItem" && obj.args.length > 0) {
            this.availSkills = [];
            this.refreshSelectedItem(obj.args[2]);
            this.fillAvailSkills();
        }

        result = this.fillTemplate();
        this.updateDb();
        return result;
    }

    refreshSelectedItem(id) {
        let filteredItems = this.serverItems.filter((el) => {
                return el.objId.toString() === id.toString();
            });
            if (filteredItems.length > 0) {
                 this.selectedItem = filteredItems[0];
            }
    }

    fillAvailSkills() {
        let itemId = this.selectedItem.objId;
        let partId = this.selectedItem.bodyPart;
        let isMagic = this.selectedItem.isMagic;
        let availSkillsIds = SkillRndManager.getAvailSkillsForPartId(partId, isMagic);

        availSkillsIds.forEach((id) => {
            this.availSkills.push(SkillRndManager.getSkillForId(id));
        });
    }
    setItemsToShow() {
        let topLimit = Math.ceil(this.serverItems.length / 6);
        if (this.obj.args.length > 0 && this.obj.args[2] === "up" && this.currentStartLine > 0) {
            this.currentStartLine--;
        }
        if (this.obj.args.length > 0 && this.obj.args[2] === "down" && this.currentStartLine < topLimit - 2) {
            this.currentStartLine++;
        }

        let clonedItems = _.clone(this.serverItems);
        let chunkedItems = _.chunk(clonedItems, 6);
        let chunkedItemsArray = _.toArray(chunkedItems);
        let shapedItemsArray = chunkedItemsArray.slice(this.currentStartLine, this.currentStartLine + 2);
        this.data["items"] = shapedItemsArray;
    }

    fillTemplate() {
        let result = "";
        let temp = fs.readFileSync("./src/html/help.html", "utf8");
        let template = handlebars.compile(temp);

        handlebars.registerHelper('lvl', function(data) {
            if (data) {
                return new handlebars.SafeString (", lvl " + data);
            };
        });

        handlebars.registerHelper('num', function numberWithCommas(x) {
            if (x) {
                return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            }
        });

        this.data["selectedItem"] = this.selectedItem;
        this.data["availSkills"] = this.availSkills;
        result = template(this.data);
        return result;
    }
}