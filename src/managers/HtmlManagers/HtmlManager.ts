import { ICommunicationObject } from "../../common/ICommunicationObject";
import * as fs from "fs";
import * as handlebars from "handlebars";
import * as _ from "lodash";
import { Db } from "../CommonManagers/DbManager";
import { SkillRndManager } from '../CommonManagers/SkillRndManager';
import { IDiabloSkill, IDiabloItem, IDb, IMainHtmlData } from '../../entities/interfaces';
import * as Configs from "../../configurations";

export class HtmlManager {
    currentStartLine:number = 0;
    data: IMainHtmlData;
    obj:ICommunicationObject;
    selectedItem:IDiabloItem;
    selectedSkill:IDiabloSkill;
    randomSkills:IDiabloSkill[] = [];
    serverItems:any[];
    playerId;
    currencyId:number = 57;
    isAdmin:boolean = false;
    config = Configs.getCfg().get("statsRndManager");
    /**
     *
     */
    constructor(playerId) {
        this.playerId = playerId;
        let data = Db.get(playerId);
        if (data) {
            this.currentStartLine = data.currentStartLine;
            this.data = data.data;
            this.selectedItem = data.selectedItem;
            this.selectedSkill = data.selectedSkill;
            this.randomSkills = data.randomSkills;
            this.currencyId = data.currencyId;
        }
        if (!this.currencyId) {
            this.currencyId = 57;
        }
    }
    updateDb() {
        let item = {
            playerId:this.playerId,
            currentStartLine:this.currentStartLine,
            data:this.data,
            selectedItem:this.selectedItem,
            selectedSkill:this.selectedSkill,
            randomSkills:this.randomSkills,
            currencyId:this.currencyId
        }
        Db.add(item);
    }
    getHtml(obj: ICommunicationObject): string {
        this.obj = obj;
        this.isAdmin = obj.hash.isAdmin === 'true';
        let lvlCheck = "";
        lvlCheck = this.checkLevel(obj.hash.lvl);
        if (!!lvlCheck) {
            return lvlCheck;
        }

        if (obj.hash.items) {
            this.serverItems = JSON.parse(obj.hash.items);
        }
        // else {
        //     this.serverItems = this.items();
        // }
        let result = "";
        // Первый аргумент - всегда тип валюты, второй - стоимость действия, если бесплатно - 0
        if (obj.args.length > 0 && obj.args[0]) {
            this.currencyId = +obj.args[0];
        }
        this.data = { money:this.obj.hash.currentAdena, error:"", currencyId:this.currencyId }; //obj.hash;
        if (obj.hash.error) {
            this.data.error = obj.hash.error;
        }
        this.setItemsToShow();
        this.randomSkills = [];
        if (obj.method === "setItem" && obj.args.length > 2) {
            this.refreshSelectedItem(obj.args[2], true);
        }

        if (obj.method === "setSkil" && obj.args.length > 2) {
            let filteredSkills = this.selectedItem != null ? this.selectedItem.skills.filter((el) => {
                return el.id.toString() === obj.args[2].toString();
            }) : [];
            if (filteredSkills.length > 0) {
                 this.selectedSkill = filteredSkills[0];
            }
        }

        if (obj.method === "doRandom" && obj.args.length > 0) {
            if (this.selectedItem && this.selectedSkill) {
                if (!obj.hash.error) {
                    this.randomSkills = SkillRndManager.getRandomSkillsForItem(this.selectedItem, this.selectedSkill) ;
                    this.checkSkills();
                }
                // else {
                //     for (let i = 0; i < 6; i++) {
                //         let skill = this.rndSkills()[this.randomIntFromInterval(0, this.rndSkills().length - 1)];
                //         let lvl = this.randomIntFromInterval(1, 50);
                //         skill.lvl = lvl;
                //         this.randomSkills.push(skill);
                //     }
                //     this.data.money = this.data.money - this.selectedItem.currentPrice;
                //     this.selectedItem.currentPrice = this.selectedItem.currentPrice * this.data.mul;
                // }
            } else {
                this.data.error = "Выберете скил для изменения!!";
            }
        }
        if (obj.method === "setSkill") {
            let items = this.serverItems.filter((ele) => {
                return ele.objId === this.selectedItem.objId;
            });
            if (items.length > 0) {
                let newItem = items[0];
                this.selectedItem = newItem;
            }
            this.selectedSkill = null;
        }
        if (this.selectedItem) {
            this.refreshSelectedItem(this.selectedItem.objId);
        }

        result = this.fillTemplate();
        this.updateDb();
        return result;
    }

    checkLevel(level): string {
        if (!level) {
            return "";
        };
        let targetLvl = this.config.itemsCBLvls[0];
        if (+level < targetLvl) {
            let result = "";
            let temp = fs.readFileSync("./src/html/lowLevel.html", "utf8");
            let template = handlebars.compile(temp);
            let data = {lvl:+level, cLvl: targetLvl.toString()};
            result = template(data);
            return result;
        } else {
            return "";
        }
    }

    checkSkills() {
        let error = [];
        let mbError = [];
        this.randomSkills.forEach((skill) => {
            if (skill.id !== this.selectedSkill.id) {
                 mbError = this.selectedItem.skills.filter((itemSkill) => {
                    return itemSkill.id === skill.id
                });
                mbError.forEach((er) => {
                    error.push(er);
                    _.remove(this.randomSkills, (sk) => sk.id === er.id);
                });
            }
        });
        if (error.length > 0) {
            this.data.error = 'В рандоме есть скилл(ы), которые уже есть в вещи!'
            let good = this.randomSkills.filter((skill) => {
                return skill.id
            })
        }
    }

    refreshSelectedItem(id, setSkillEmpty?) {
        let clone = _.clone(this.serverItems);
        let filteredItems = clone.filter((el) => {
                return el.objId.toString() === id.toString();
            });
            if (filteredItems.length > 0) {
                let item:IDiabloItem = filteredItems[0];
                item.currentPrice = SkillRndManager.getRndPrice(this.currencyId, item.currentRandom, item.grade)
                this.selectedItem = item;
            } else {
                this.selectedItem = null;
            }
            if (setSkillEmpty) {
                this.selectedSkill = null;
            }
    }

    setItemsToShow() {
        let topLimit = Math.ceil(this.serverItems.length / 6);
        if (this.obj.args.length > 0 && this.obj.args[2] === "up" && this.currentStartLine > 0) {
            this.currentStartLine--;
        }
        if (this.obj.args.length > 0 && this.obj.args[2] === "down" && this.currentStartLine < topLimit - 2) {
            this.currentStartLine++;
        }

        let clonedItems = _.clone(this.serverItems);
        let chunkedItems = _.chunk(clonedItems, 6);
        let chunkedItemsArray = _.toArray(chunkedItems);
        let shapedItemsArray = chunkedItemsArray.slice(this.currentStartLine, this.currentStartLine + 2);
        this.data["items"] = shapedItemsArray;
    }

    fillTemplate() {
        let result = "";
        let temp = fs.readFileSync("./src/html/main.html", "utf8");
        let template = handlebars.compile(temp);

        handlebars.registerHelper('lvl', function(data) {
            if (data) {
                return new handlebars.SafeString (", lvl " + data);
            };
        });

        handlebars.registerHelper('num', function numberWithCommas(x) {
            if (x) {
                return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            }
        });

        this.data["selectedItem"] = this.selectedItem;
        this.data["selectedSkill"] = this.selectedSkill;
        this.data["randomSkills"] = this.randomSkills;
        this.data["isAdmin"] = this.isAdmin;
        result = template(this.data);
        return result;
    }
}