import { ICommunicationObject } from "../../common/ICommunicationObject";
import * as fs from "fs";
import * as handlebars from "handlebars";
import * as _ from "lodash";
import { Db } from "../CommonManagers/DbManager";
import { XmlManager } from "../DataManagers/XmlManager";
import * as Configs from "../../configurations";
import { SkillRndManager } from "../CommonManagers/SkillRndManager";
import { SkillEnchantManager } from "../CommonManagers/SkillEnchantManager";
import { IDb, IEnchantHtmlData, IDiabloSkill, IDiabloItem } from '../../entities/interfaces';
import { UtilManager } from "../CommonManagers/UtilManager";

export class HtmlEnchantManager {
    currentStartLine:number = 0;
    data:IEnchantHtmlData;
    obj:ICommunicationObject;
    selectedItem:IDiabloItem;
    availSkills = [];
    serverItems:any[];
    playerId;
    selectedSkill:IDiabloSkill;
    currencyId:number;
    currencyIcon:string;
    currentPrice:number;
    moneyAmount:number;
    /**
     *
     */
    constructor(playerId) {
        this.currencyIcon = "Etc_coin_of_fair_i00_0";
        this.playerId = playerId;
        let data = Db.get(playerId + "enchant");
        if (data) {
            this.currentStartLine = data.currentStartLine;
            this.data = data.data;
            this.selectedItem = data.selectedItem;
            this.availSkills = data.randomSkills;
            this.selectedSkill = data.selectedSkill;
            this.currencyId = data.currencyId;
            this.currentPrice = data.currentPrice;
        } else {
            this.data = {} as IEnchantHtmlData;
        }
        if (!this.currencyId) {
            this.currencyId = 6673;
        }
        if (!this.currentPrice) {
            this.currentPrice = 1;
        }
        this.data.halfPrice = Math.round(this.currentPrice / 2);
    }
    updateDb() {
        let item = {
            playerId:this.playerId + "enchant",
            currentStartLine:this.currentStartLine,
            data:this.data,
            selectedItem:this.selectedItem,
            availSkills:this.availSkills,
            selectedSkill:this.selectedSkill,
            currencyId:this.currencyId,
            currentPrice:this.currentPrice
        }
        Db.add(item);
    }
    getHtml(obj: ICommunicationObject): string {
        this.obj = obj;
        if (obj.hash.items) {
            this.serverItems = JSON.parse(obj.hash.items);
        }
        if (obj.hash.currencyAmount) {
            this.data.money = obj.hash.currencyAmount;
        } else {
            this.data.money = 0;
        }
        let result = "";
        this.data.error = obj.hash.error || "";
        if (this.data.error) {
            this.selectedItem = null;
            this.selectedSkill = null;
        }
        this.data.successChance = "";
        this.data.failChance = "";
        this.data.del = "";
        this.data.checkUpgradeDel = "-1";
        this.data.delShow = null;
        this.setItemsToShow();
        this.data.showResult = false;
        if (obj.hash.upgrade != null) {
            if (obj.hash.upgrade === "ok") {
                this.refreshSelectedItem(this.selectedItem.objId, false, true);
            } else if (obj.hash.upgrade === "notok") {
                this.selectedItem = null;
                this.selectedSkill = null;
            } else if (obj.hash.upgrade === "showResult") {
                this.data.showResult = true;
            }
        }
        if (obj.method === "setEnchantItem" && obj.args.length > 0) {
            this.availSkills = [];
            this.refreshSelectedItem(obj.args[2], true);
        }

        if (obj.method === "setEnchantSkill" && obj.args.length > 2) {
            let filteredSkills = this.selectedItem != null ? this.selectedItem.skills.filter((el) => {
                return el.id.toString() === obj.args[2].toString();
            }) : [];
            if (filteredSkills.length > 0) {
                 this.selectedSkill = filteredSkills[0];
                 this.data.uplvl = this.selectedSkill.lvl < 50 ? this.selectedSkill.lvl + 1 : this.selectedSkill.lvl;
            }
        }

        if (obj.method === "enchantModifyPrice" && obj.args.length > 2) {
            let mod = +obj.args[2];
            if (this.selectedItem != null) {
                if (this.selectedSkill != null) {
                    if (mod === 1 && this.data.money > this.currentPrice) {
                        this.currentPrice++;
                    } else if (this.data.money < this.currentPrice) {
                        this.data.error = "Не хватает денег!";
                    }
                    if (mod === -1 && this.currentPrice > 0) {
                        this.currentPrice--;
                    }
                } else {
                    this.data.error = "Нужно выбрать скил";
                }
            } else {
                this.data.error = "Нужно выбрать итем";
            }
        }

        if (obj.method === "enchantSetPrice" && obj.args.length > 2) {
            let set = +obj.args[2];
            if (this.selectedItem != null) {
                if (this.selectedSkill != null) {
                    if (this.data.money >= set) {
                        this.currentPrice = set;
                    } else {
                        this.data.error = "Не хватает денег!";
                    }
                } else {
                    this.data.error = "Нужно выбрать скил";
                }
            } else {
                this.data.error = "Нужно выбрать итем";
            }
        }

        this.setEnchantChances();

        result = this.fillTemplate();
        this.updateDb();
        return result;
    }

    setEnchantChances() {
        if (this.selectedItem && this.selectedSkill && !this.data.error) {
            let chances = SkillEnchantManager.getChances(this.selectedItem.grade,
                this.selectedSkill.lvl, this.currentPrice, this.currencyId);
            this.data.successChance = chances.success;
            this.data.failChance = chances.fail;

            if (this.currentPrice > chances.maxPrice) {
                this.currentPrice = chances.maxPrice;
            }

            let rnd = UtilManager.randomIntFromInterval(1, 100);
            let del = rnd <= chances.fail ? "1" : "0";
            this.data.del = del;
            if (del === "1") {
                this.data.delShow = true;
            } else {
                this.data.delShow = false;
            }
        }
    }

    refreshSelectedItem(id, setSkillEmpty?, refreshSkill?) {
        let filteredItems = this.serverItems.filter((el) => {
                return el.objId.toString() === id.toString();
            });
            if (filteredItems.length > 0) {
                 this.selectedItem = filteredItems[0];
            }
            if (setSkillEmpty) {
                this.selectedSkill = null;
            }
            if (refreshSkill && this.selectedSkill) {
                let filteredSkills = this.selectedItem != null ? this.selectedItem.skills.filter((el) => {
                    return el.id.toString() === this.selectedSkill.id.toString();
                }) : [];
                if (filteredSkills.length > 0) {
                    this.selectedSkill = filteredSkills[0];
                    this.data.uplvl = this.selectedSkill.lvl < 50 ? this.selectedSkill.lvl + 1 : this.selectedSkill.lvl;
                }
            }
    }

    setItemsToShow() {
        let topLimit = Math.ceil(this.serverItems.length / 6);
        if (this.obj.args.length > 0 && this.obj.args[2] === "up" && this.currentStartLine > 0) {
            this.currentStartLine--;
        }
        if (this.obj.args.length > 0 && this.obj.args[2] === "down" && this.currentStartLine < topLimit - 2) {
            this.currentStartLine++;
        }

        let clonedItems = _.clone(this.serverItems);
        let chunkedItems = _.chunk(clonedItems, 6);
        let chunkedItemsArray = _.toArray(chunkedItems);
        let shapedItemsArray = chunkedItemsArray.slice(this.currentStartLine, this.currentStartLine + 2);
        this.data["items"] = shapedItemsArray;
    }

    fillTemplate() {
        let result = "";
        let temp = fs.readFileSync("./src/html/enchant.html", "utf8");
        let template = handlebars.compile(temp);

        handlebars.registerHelper('lvl', function(data) {
            if (data) {
                return new handlebars.SafeString (", lvl " + data);
            };
        });

        handlebars.registerHelper('num', function numberWithCommas(x) {
            if (x) {
                return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            }
        });
        this.data.showSuccess = this.data.showResult && !this.data.delShow;
        this.data.showFail = this.data.showResult && this.data.delShow;
        this.data["selectedItem"] = this.selectedItem;
        this.data["selectedSkill"] = this.selectedSkill;
        this.data["currencyId"] = this.currencyId;
        this.data["currentPrice"] = this.currentPrice;
        result = template(this.data);
        return result;
    }
}