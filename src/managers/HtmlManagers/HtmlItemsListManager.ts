import { ICommunicationObject } from "../../common/ICommunicationObject";
import * as fs from "fs";
import * as handlebars from "handlebars";
import * as _ from "lodash";
import { Db } from "../CommonManagers/DbManager";
import { SkillRndManager } from '../CommonManagers/SkillRndManager';
import { IDiabloSkill, IDiabloItem, IDb, IMainHtmlData } from '../../entities/interfaces';
import * as Configs from "../../configurations";

export class HtmlItemsListManager {
    currentStartLine:number = 0;
    data: any;
    obj:ICommunicationObject;
    serverItems:any[];
    playerId;
    selectedItem;
    config = Configs.getCfg().get("statsRndManager");
    /**
     *
     */
    constructor(playerId) {
        this.playerId = playerId;
        let data = Db.get(playerId);
        if (data) {
            this.currentStartLine = data.currentStartLine;
            this.selectedItem = data.selectedItem;
            this.data = data.data;
        }
    }
    updateDb() {
        let item = {
            playerId:this.playerId,
            currentStartLine:this.currentStartLine,
            selectedItem:this.selectedItem,
            data:this.data,
        }
        Db.add(item);
    }
    getHtml(obj: ICommunicationObject): string {
        this.obj = obj;
        if (obj.hash.items) {
            this.serverItems = JSON.parse(obj.hash.items);
        }
        let result = "";
        this.data = {};
        if (obj.hash.error) {
            this.data.error = obj.hash.error;
        }
        this.setItemsToShow();

        if ((obj.method === "itemsListSetItem"
            || obj.method === "itemsListEqUneq"
            || obj.method === "itemsListDrop"
            || obj.method === "itemsListSell"
            || obj.method === "itemsListWh"
        ) && obj.args.length > 0) {
            this.refreshSelectedItem(obj.args[0]);
        }

        result = this.fillTemplate();
        this.updateDb();
        return result;
    }
    refreshSelectedItem(id) {
        let filteredItems = this.serverItems.filter((el) => {
                return el.objId.toString() === id.toString();
            });
            if (filteredItems.length > 0) {
                 this.selectedItem = filteredItems[0];
            } else {
                this.selectedItem = null;
            }
    }

    setItemsToShow() {
        let topLimit = Math.ceil(this.serverItems.length / 12);
        if (this.obj.args.length > 0 && this.obj.args[0] === "up" && this.currentStartLine > 0) {
            this.currentStartLine--;
        }
        if (this.obj.args.length > 0 && this.obj.args[0] === "down" && this.currentStartLine < topLimit - 2) {
            this.currentStartLine++;
        }

        let clonedItems = _.clone(this.serverItems);
        let chunkedItems = _.chunk(clonedItems, 12);
        let chunkedItemsArray = _.toArray(chunkedItems);
        let shapedItemsArray = chunkedItemsArray.slice(this.currentStartLine, this.currentStartLine + 2);
        this.data["items"] = shapedItemsArray;
    }

    fillTemplate() {
        let result = "";
        let temp = fs.readFileSync("./src/html/itemslist.html", "utf8");
        let template = handlebars.compile(temp);

        handlebars.registerHelper('lvl', function(data) {
            if (data) {
                return new handlebars.SafeString (", lvl " + data);
            };
        });

        handlebars.registerHelper('num', function numberWithCommas(x) {
            if (x) {
                return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            }
        });
        this.data["selectedItem"] = this.selectedItem;
        result = template(this.data);
        return result;
    }
}