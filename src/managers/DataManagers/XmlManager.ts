import * as fs from "fs";
import * as w from "../../common/Logger";
import * as xml2js from "xml2js";

export class XmlManager {
    public static skills = [];
    static parser = new xml2js.Parser();
    public static getXml(type):string {
        let result = "";
        let fileContents = [];
        let dirName = "./src/xml/" + type + "/";
        try {
            let fileNames = fs.readdirSync(dirName);
            for (var i = 0; i < fileNames.length; i++) {
                let fileName = fileNames[i];
                let fc = fs.readFileSync(dirName + fileName, "utf-8");
                fileContents.push(fc);
            }
        } catch (ex) {
            w.getLogger().error(ex);
        } finally {
            result = JSON.stringify(fileContents);
        }
        this.getSkills();
        return result;
    }
    public static getSkills() {
        this.skills = [];
        let dirName = "./src/xml/skills/";
        try {
            let fileNames = fs.readdirSync(dirName);
            for (var i = 0; i < fileNames.length; i++) {
                let fileName = fileNames[i];
                let fc = fs.readFileSync(dirName + fileName, "utf-8");
                this.parser.parseString(fc, (err, data) => {
                    if (err) {
                        w.getLogger().error("err");
                    }
                    this.skills.push(data);
                });
            }
        } catch (ex) {
            w.getLogger().error(ex);
        } finally {
        }
    }
}