import * as fs from "fs";
import * as w from "../../common/Logger";
import * as path from "path";

export class HtmlCacheManager {
    public static getHtml(locale):string {
        let result = "";
        let fileContents = [];
        let dirName = "./src/htmlChache/" + locale + "/";
        try {
            let fileNames = this.walkSync(dirName);
            for (var i = 0; i < fileNames.length; i++) {
                let fileName = fileNames[i];
                fileContents.push({
                    contents:fs.readFileSync(dirName + fileName, "utf-8"),
                    locale:locale,
                    path:fileName
                });
            }
        } catch (ex) {
            w.getLogger().error(ex);
        } finally {
            result = JSON.stringify(fileContents);
        }
        return result;
    }

    static walkSync (dir, filelist?) {
    fs.readdirSync(dir).forEach(file => {
        filelist = fs.statSync(path.join(dir, file)).isDirectory()
        ? this.walkSync(path.join(dir, file), filelist)
        : filelist.concat(path.join(dir, file));
        });
        return filelist;
    }
}

