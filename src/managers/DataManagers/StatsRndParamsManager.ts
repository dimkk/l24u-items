import * as w from "../../common/Logger";
import * as Configs from "../../configurations";

export class StatsRndParamsManager {
    public static getParams():string {
        let param;
        let result;
        try {
           param = Configs.getCfg().get("statsRndManager");
        } catch (ex) {
            w.getLogger().error(ex);
        } finally {
            result = JSON.stringify(param);
        }
        return result;
    }
}