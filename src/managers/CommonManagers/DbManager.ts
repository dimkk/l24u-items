import { ICommunicationObject } from "../../common/ICommunicationObject";
import { IDb } from "../../entities/interfaces";
import * as _ from "lodash";

export class Db {
    static db: IDb[] = [];

    /**
     * Добавляет (если такой есть убирает) итем из базы
     */
    public static add(item:IDb) {
        _.remove(this.db, (elem) => {
            return elem.playerId === item.playerId;
        });
        this.db.push(item);
    }
    /**
     * Достаёт итем из базы
     */
    public static get(id) {
        let items = _.filter(this.db, (elem) => {
            return elem.playerId === id;
        });
        if (items.length > 0) {
            return items[0];
        } else {
            return null;
        }

    }
}