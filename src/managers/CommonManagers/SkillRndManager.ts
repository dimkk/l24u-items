import * as _ from "lodash";
import { XmlManager } from "../DataManagers/XmlManager";
import * as Configs from "../../configurations";
import { ICommunicationObject } from "../../common/ICommunicationObject";
import { IDiabloSkill, IDiabloItem } from '../../entities/interfaces';
import { UtilManager } from './UtilManager';

export class SkillRndManager {
    static config = Configs.getCfg().get("statsRndManager");
    static randomIntFromInterval = UtilManager.randomIntFromInterval;

    public static getRndPrice(currencyId:number, randomCount:number, grade:string):number {
        let price = 0;

        if (grade === "NONE") {
            price = this.config.randomStartPrices[0];
        }
        if (grade === "D") {
            price = this.config.randomStartPrices[1];
        }
        if (grade === "C") {
            price = this.config.randomStartPrices[2];
        }
        if (grade === "B") {
            price = this.config.randomStartPrices[3];
        }
        if (grade === "A") {
            price = this.config.randomStartPrices[4];
        }
        if (grade === "S") {
            price = this.config.randomStartPrices[5];
        }
        if (grade === "S80") {
            price = this.config.randomStartPrices[6];
        }
        if (grade === "S84") {
            price = this.config.randomStartPrices[7];
        }

        if (randomCount !== 0) {
            let temp = (price * 1.5) * randomCount;
            price = Math.round(temp);

        }
        return price;
    }

    public static getRandomSkillsForItem(item:IDiabloItem, skillToInclude:IDiabloSkill):IDiabloSkill[] {
        let result: IDiabloSkill[] = [];
        let filteredSkills = this.getAvailSkillIdsForItem(item, skillToInclude.id);
        result.push(skillToInclude);
        for (let i = 0; i < 5; i++) {
            let randomSkillArrayIndex = this.randomIntFromInterval(0, filteredSkills.length - 1);
            let skill = filteredSkills[randomSkillArrayIndex];
            let skillWithOffset = this.getSkillForId(skill + this.getOffsetForBodyPart(item.bodyPart),
                                                        this.getRandomSkillLvl(this.config.skillLvlChances));
            result.push(skillWithOffset);
        }
        return result;
    }

    public static getAvailSkillsForItem(item:IDiabloItem, skillToInclude:IDiabloSkill):IDiabloSkill[] {
        let result: IDiabloSkill[] = [];
        let filteredSkills = this.getAvailSkillIdsForItem(item, skillToInclude ? skillToInclude.id : 0);
        //result.push(skillToInclude);
        filteredSkills.forEach((id) => {
            let skillWithOffset = this.getSkillForId(id + this.getOffsetForBodyPart(item.bodyPart), '50');
            result.push(skillWithOffset);
        });
        return result;
    }

    public static getAvailSkillIdsForItem(item:IDiabloItem, skillIdToInclude:number):number[] {
        let availSkillsIds = this.getAvailSkillsForPartId(item.bodyPart, item.isMagic);
        let excludedSkillIds = _.chain(item.skills)
                                .filter((el) => el.id !== skillIdToInclude)
                                .map((el) => (el.id - this.getOffsetForBodyPart(item.bodyPart)))
                                .value();
        let filteredSkills = availSkillsIds.filter((id) => excludedSkillIds.indexOf(id) === -1);
        return filteredSkills;
    }

    static getRandomSkillLvl(skillLvlChances) {
        let topLvl = this.randomIntFromInterval(0, skillLvlChances[4]) <= skillLvlChances[0];
        if (topLvl) {
            return this.randomIntFromInterval(41, 50);
        }
        let highLvl = this.randomIntFromInterval(0, skillLvlChances[4]) <= skillLvlChances[1];
        if (highLvl) {
            return this.randomIntFromInterval(31, 40);
        }
        let midLvl = this.randomIntFromInterval(0, skillLvlChances[4]) <= skillLvlChances[2];
        if (midLvl) {
            return this.randomIntFromInterval(21, 30);
        }
        let lowLvl = this.randomIntFromInterval(0, skillLvlChances[4]) <= skillLvlChances[3];
        if (lowLvl) {
            return this.randomIntFromInterval(11, 20);
        }
        return this.randomIntFromInterval(1, 10);
    }

    static getOffsetForBodyPart(partId) {
        let offset = 0;

        if (partId === 64) {
            offset = this.config.headOffset;
        }

        if (partId === 1024 || partId === 32768) {
            offset = this.config.bodyOffset;
        }

        if (partId === 512) {
            offset = this.config.glovesOffset;
        }

        if (partId === 4096) {
            offset = this.config.shoesOffset;
        }
        return offset;
    }

    public static getAvailSkillsForPartId(partId, isMagic):number[] {
        let cfg = this.config;
        let availSkillsIds = [];

        if (partId === 128 || partId === 16384) {
            if (isMagic) {
                availSkillsIds = cfg.mWeapSkills;
            } else {
                availSkillsIds = cfg.pWeapSkills;
            }
        }

        if (partId === 64) {
            if (isMagic) {
                availSkillsIds = cfg.mHeadSkills;
            } else {
                availSkillsIds = cfg.pHeadSkills;
            }
        }

        if (partId === 1024 || partId === 32768) {
            if (isMagic) {
                availSkillsIds = cfg.mBodySkills;
            } else {
                availSkillsIds = cfg.pBodySkills;
            }
        }

        if (partId === 512) {
            if (isMagic) {
                availSkillsIds = cfg.mGlovesSkills;
            } else {
                availSkillsIds = cfg.pGlovesSkills;
            }
        }

        if (partId === 4096) {
            if (isMagic) {
                availSkillsIds = cfg.mShoesSkills;
            } else {
                availSkillsIds = cfg.pShoesSkills;
            }
        }
        return availSkillsIds;
    }

    public static getSkillForId(id, skilllvl?) {
        let skill;
        XmlManager.skills.forEach((el) => {
            let rawSkills = el.list.skill;
            let filteredRawSkills = rawSkills.filter((sEl) => {
                return sEl.$.id.toString() === id.toString();
            });
            if (filteredRawSkills.length > 0) {
                let rawSkill = filteredRawSkills[0];
                skill = {
                    id:rawSkill.$.id,
                    name:rawSkill.$.name,
                    icon:rawSkill.set[0] != null ? rawSkill.set[0].$.val : "",
                    lvl:skilllvl || rawSkill.$.levels
                }
            }
        });
        return skill;
    }
}