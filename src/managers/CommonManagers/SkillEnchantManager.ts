import * as _ from "lodash";
import {XmlManager} from "../DataManagers/XmlManager";
import * as Configs from "../../configurations";
import { ICommunicationObject } from "../../common/ICommunicationObject";
import { IDiabloSkill, IDiabloItem } from '../../entities/interfaces';
import { UtilManager } from './UtilManager';

export class SkillEnchantManager {
    static config = Configs.getCfg().get("statsRndManager");
    //                                                                   ng, d, c, b,  a,  s,  s80,s84
    static gradeBasePrice = SkillEnchantManager.config.gradeBasePrice || [50, 70, 90, 110, 130, 150, 250, 500];
    static baseSuccessChance = SkillEnchantManager.config.baseSuccessChance || 67;
    //                      11+, 21+, 31+, 41+ 
    static skillLvlAddon = SkillEnchantManager.config.skillLvlAddon || [
        [0, 0, 0, 0, 0, 0, 0, 0],
        //11+
        [10, 14, 18, 22, 26, 30, 50, 100],
        //21+
        [15, 21, 27, 33, 39, 45, 75, 150],
        //31+
        [20, 28, 36, 44, 52, 60, 100, 200],
        //41+
        [25, 35, 45, 55, 65, 75, 125, 250],
    ]
    static gradeEnum = [
        { id:0, grade:"NONE" }, { id:1, grade:"D" }, { id:2, grade:"C" },
        { id:3, grade:"B" }, { id:4, grade:"A" }, { id:5, grade:"S" },
        { id:6, grade:"S80" }, { id:7, grade:"S84" }
    ]
    static lvlEnum = [
        { id:0, min:1, max:10 },
        { id:1, min:11, max:20 }, { id:2, min:21, max:30 },
        { id:3, min:31, max:40 }, { id:4, min:41, max:50 }
    ]
    public static getChances(grade:string, lvl:number, price:number, currencyId:number):any {
        let result = 0;
        let planPrice = this.gradeBasePrice[this.getEnumGrade(grade)];
        let addOn = this.skillLvlAddon[this.getEnumLvl(lvl)][this.getEnumGrade(grade)] +
                 this.getEnumLvl(lvl) === 4 ? lvl : Math.round(lvl / 2);
        let fullPrice = planPrice + addOn;
        let rel = price / fullPrice;
        result = Math.round(rel * this.baseSuccessChance);
        if (result > 90) {
            result = 90;
        }

        let maxPrice = Math.round((90 / this.baseSuccessChance) * fullPrice); // = (price / fullPrice) * this.baseSuccessChance;

        return { success: result, fail: 100 - result, maxPrice:maxPrice };
    }
    private static getEnumGrade(grade) {
        let en = this.gradeEnum.find((el) => el.grade === grade);
        return en.id;
    }
    private static getEnumLvl(lvl) {
        let en = this.lvlEnum.find((el) => lvl >= el.min && lvl <= el.max);
        return en.id;
    }
}