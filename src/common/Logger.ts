let winston = require("winston");

winston.configure({
    transports: [
      new (winston.transports.Console)({'timestamp':true}),
      new (winston.transports.File)({ filename: 'somefile.log',  })
    ]
  });

export function getLogger() {
    return winston;
}